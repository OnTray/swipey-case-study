# Demo for Swipey interview case study
Technical test, testing out the `nba` site and a `football` API

## Test details
For details on how the tests are designed, please [see the wiki](https://gitlab.com/OnTray/swipey-case-study/-/wikis/home)

## Running tests locally
1. Prerequisites
    - `Git`
    - `npm`
2. Clone repository to your local machine
```
git clone https://gitlab.com/OnTray/swipey-case-study.git
```
3. Get an API key from the football site and store it in a `.env` file under the key `FOOTBALL_KEY`. If you're here from Swipey, refer to the email as I already have a key
```
FOOTBALL_KEY=<your_api_key_here>
```
4. Install npm dependencies
```
npm install
```
5. Install browsers for playwright
```
npx playwright install
```
6. Run tests
```
npx playwright test
```

See [Playwright documentation](https://playwright.dev/docs/test-cli ) for more information
