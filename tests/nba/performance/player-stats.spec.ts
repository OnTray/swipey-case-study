import test, { expect } from "@playwright/test";
import { StatsPlayerPage } from "../pom/nba-pom"

test('Player page stats should load within 4s', async ({ page }) => {
    // Stats locator
    const stats = page.locator('table:has-text("OVERALL")');

    // Goto page
    await page.goto('https://www.nba.com/stats/player/203954/traditional', { waitUntil: "commit" });

    // Check that stats page loaded in 4s
    await expect(stats, { message: 'Ensure stats are loaded within 4s' }).toBeVisible({ timeout: 4 * 1000 });
});