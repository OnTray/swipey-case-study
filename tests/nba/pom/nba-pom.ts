import { Locator, Page } from "@playwright/test";

export class StatsPlayerPage {
    /** 
     * Page to be used for loading and retrieving the site
     */
    readonly page: Page;

    /** 
     * ID of the player, needed to determine the page
     */
    readonly playerId: string;

    /**
     * Points per game
     */
    readonly statPpg: Locator;

    /** 
     * Rebounds per game 
     */
    readonly statRpg: Locator;

    /**
     * Assists per game
     */
    readonly statApg: Locator;

    constructor(page: Page, playerId?: string) {
        this.page = page;

        if (playerId == undefined) {
            playerId = page.url().replace(/\D/g, '');
        } else {
            this.playerId = playerId;
        }

        // Stats are found by: Label (p) > Parent > Value (2nd p in parent)
        this.statPpg = page.locator('p:has-text("PPG")').locator('..').locator('p >> nth=1');
        this.statRpg = page.locator('p:has-text("RPG")').locator('..').locator('p >> nth=1');
        this.statApg = page.locator('p:has-text("APG")').locator('..').locator('p >> nth=1');
    }

    async goto() {
        await this.page.goto('https://www.nba.com/stats/player/' + this.playerId + '/traditional');
    }
}

export class StatsLeadersPage {
    /** 
     * Page to be used for loading and retrieving the site
     */
    readonly page: Page;

    /**
     * Table body of leaders
     */
    readonly leadersTableBody: Locator;

    constructor(page: Page) {
        this.page = page;
        this.leadersTableBody = page.locator('table:has-text("PLAYER")').locator('tbody');
    }

    /**
     * Goto /stats/leaders page
     */
    async goto() {
        await this.page.goto('https://www.nba.com/stats/leaders');
    }

    /**
     * Get the row locator
     * @param index Index of row to retrieve
     * @returns Locator of the row
     */
    getRow(index: number): Locator {
        return this.leadersTableBody.locator('tr').nth(index);
    }

    /**
     * Go to player page
     * @param row Row locator of player to go to
     */
    async gotoPlayer(row: Locator) {
        const player = Utility.getLocatorFromTableRow(row, 1).locator('a');
        await player.click();
    }

    /**
     * Get the player name from a row
     * @param row Row locator to retrieve from
     * @returns Player name from a row
     */
    async getName(row: Locator): Promise<string> {
        return Utility.getDataFromTableRow(row, 1);
    }

    /**
     * Get the player points from a row
     * @param row Row locator to retrieve from
     * @returns Player points from a row
     */
    async getPoints(row: Locator): Promise<string> {
        return Utility.getDataFromTableRow(row, 4);
    }

    /**
     * Get the player rebounds from a row
     * @param row Row locator to retrieve from
     * @returns Player rebounds from a row
     */
    async getRebounds(row: Locator): Promise<string> {
        return Utility.getDataFromTableRow(row, 16);
    }

    /**
     * Get the player assists from a row
     * @param row Row locator to retrieve from
     * @returns Player assists from a row
     */
    async getAssists(row: Locator): Promise<string> {
        return Utility.getDataFromTableRow(row, 17);
    }
}

export class StatsTeamPlayersPage {
    /** 
     * Page to be used for loading and retrieving the site
     */
    readonly page: Page;

    /** 
     * ID of the team, needed to determine the page
     */
    readonly teamId: string;

    /**
     * Table body of players
     */
    readonly playersTableBody: Locator;

    constructor(page: Page, teamId: string) {
        this.page = page;
        this.teamId = teamId;
        this.playersTableBody = page.locator('table:has-text("PLAYER")').locator('tbody');
    }

    async goto() {
        await this.page.goto('https://www.nba.com/stats/players/traditional?sort=PTS&dir=-1&TeamID=' + this.teamId);
    }

    /**
     * Get the row locator
     * @param index Index of row to retrieve
     * @returns Locator of the row
     */
    getRow(index: number): Locator {
        return this.playersTableBody.locator('tr').nth(index);
    }

    /**
     * Go to player page
     * @param row Row locator of player to go to
     */
    async gotoPlayer(row: Locator) {
        const player = Utility.getLocatorFromTableRow(row, 1).locator('a');
        await player.click();
    }

    /**
     * Get the player name from a row
     * @param row Row locator to retrieve from
     * @returns Player name from a row
     */
    async getName(row: Locator): Promise<string> {
        return Utility.getDataFromTableRow(row, 1);
    }

    /**
     * Get the player points from a row
     * @param row Row locator to retrieve from
     * @returns Player points from a row
     */
    async getPoints(row: Locator): Promise<string> {
        return Utility.getDataFromTableRow(row, 8);
    }
}

class Utility {
    static getLocatorFromTableRow(row: Locator, column: number): Locator {
        return row.locator('td >> nth=' + column);
    }

    static async getDataFromTableRow(row: Locator, column: number): Promise<string> {
        return (
            await Utility.getLocatorFromTableRow(row, column).innerText()
        ).trim();
    }
}