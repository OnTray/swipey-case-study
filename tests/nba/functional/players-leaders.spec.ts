import test, { expect, Page } from "@playwright/test";
import { StatsLeadersPage, StatsPlayerPage } from "../pom/nba-pom";

test('Stats of top three leaders', async ({ page }) => {
    await assertPlayerStats(page, 0);
    await assertPlayerStats(page, 1);
    await assertPlayerStats(page, 2);
});

/**
 * Assert the PPG, RPG, APG of the player
 * @param page Page to be used
 * @param index Index of player in player column list
 */
async function assertPlayerStats(page: Page, index: number) {
    /* ===== Retrieve data ====== */
    // POM, navigate to page
    const leadersPom = new StatsLeadersPage(page);
    await leadersPom.goto();

    // Retrieve row
    const playerRow = leadersPom.getRow(index);

    // Retrieve actual results
    const playerName: string = await leadersPom.getName(playerRow);
    const actualPpg: string = await leadersPom.getPoints(playerRow);
    const actualRpg: string = await leadersPom.getRebounds(playerRow);
    const actualApg: string = await leadersPom.getAssists(playerRow);

    /* ===== Retrieve expected results ====== */
    // Goto player page
    await leadersPom.gotoPlayer(playerRow);

    // POM
    const playerPom = new StatsPlayerPage(page)

    // Assert items
    expect(actualPpg, { message: 'Checking PPG of ' + playerName }).toEqual(await playerPom.statPpg.innerText());
    expect(actualRpg, { message: 'Checking RPG of ' + playerName }).toEqual(await playerPom.statRpg.innerText());
    expect(actualApg, { message: 'Checking APG of ' + playerName }).toEqual(await playerPom.statApg.innerText());
}