import test, { expect } from "@playwright/test";

test('Overall number of wins of teams should be accurate', async ({ page }) => {

    /* ===== Retrieve team wins from standings page ===== */
    // Navigate to standings (data) page to retrieve list of teams from Eastern and Western conferences
    await page.goto('https://www.nba.com/standings');

    // Test data
    let teamScore = new Map<string, string>();

    // Retrieve table body -> table rows -> table data
    const dataTeamRow = page.locator('tbody').locator('tr');
    const dataTeamRowCount = await dataTeamRow.count();
    const val = await dataTeamRow.nth(0).innerText();
    for (let i = 0; i < dataTeamRowCount; i++) {
        teamScore.set(
            // Key: Team City + Name (column 1)
            (await dataTeamRow.nth(i).locator('td >> nth=0').locator('span >> nth=1').innerText()).trim()
                + " "
                + (await dataTeamRow.nth(i).locator('td >> nth=0').locator('span >> nth=2').innerText()).trim(),
            // Value: Wins (column 2)
            await dataTeamRow.nth(i).locator('td >> nth=1').innerText()
        )
    }

    // Data assertion: Ensure data is not empty
    await expect(dataTeamRow).not.toHaveCount(0);

    /* ===== Assert stats page ===== */
    // Navigate to stats page
    await page.goto('https://www.nba.com/stats/teams/traditional?dir=A&sort=TEAM_NAME');

    // Retrieve table body
    const autTeamBody = page.locator('table:has-text("TEAM")');
    autTeamBody.waitFor();

    // Retrieve table row
    const autTeamRow = autTeamBody.locator('tbody').locator('tr');

    // Data assertion: Check that row count is at least 1
    await expect(autTeamRow).not.toHaveCount(0);

    // Data assertion: Check that each row is correct
    const autTeamRowCount = await autTeamRow.count();
    for (let i = 0; i < autTeamRowCount; i++) {
        // Retrieve data from row
        const autName = (await autTeamRow.nth(i).locator('td >> nth=1').innerText()).trim();
        const autWins = autTeamRow.nth(i).locator('td >> nth=3');

        // Retrieve data (and ensure it isn't undefined)
        let expectedScore = teamScore.get(autName);
        expect(expectedScore, {message: "Retrieve score for " + autName}).not.toBeUndefined();

        // Assert that score is equal to expected
        if (expectedScore != undefined) {
            await expect(autWins, {message: "Checking on score for " + autName}).toHaveText(expectedScore);
        }
    }
});