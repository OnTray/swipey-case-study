import test, { expect, Locator, Page } from "@playwright/test";
import { StatsPlayerPage, StatsTeamPlayersPage } from "../pom/nba-pom"

test('Average points per game of players (Golden State Warriors)', async ({ context, page }) => {
    await assertPlayer(page, '1610612744', 0);
    await assertPlayer(page, '1610612744', 4);
    await assertPlayer(page, '1610612744', 12);
});

test('Average points per game of players (Houston Rockets)', async ({ context, page }) => {
    await assertPlayer(page, '1610612745', 1);
    await assertPlayer(page, '1610612745', 7);
    await assertPlayer(page, '1610612745', 10);
});

async function assertPlayer(page: Page, teamId: string, index: number) {
    /* ===== Retrieve data ===== */
    // POM, navigate to page
    const playersPom = new StatsTeamPlayersPage(page, teamId);
    await playersPom.goto();

    // Retrieve for players
    const playerRow = playersPom.getRow(index);
    const playerName: string = await playersPom.getName(playerRow);
    const playerPoints: string = await playersPom.getPoints(playerRow);

    /* ===== Assert ===== */
    // Goto player page
    await playersPom.gotoPlayer(playerRow);

    // POM
    const playerPom = new StatsPlayerPage(page);

    // Assert
    expect(playerPoints, { message: "Checking on points-per-game for " + playerName }).toEqual(await playerPom.statPpg.innerText());
}