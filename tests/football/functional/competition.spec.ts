import test, { expect } from "@playwright/test";
import { Utility } from "../utility";

test('Competitions return accurate list', async ({ request }) => {

    const key = Utility.getKey();
    const url = 'http://api.football-data.org/v4/competitions';

    // Send request
    const response = await request.get(
        url, {
        headers: {
            'X-Auth-Token': key
        }
    });

    // Assert: Response code 200
    expect(response.status()).toEqual(200);

    // Assert: Competitions
    expect(await response.json()).toHaveProperty('competitions.2.code', "PL");
    expect(await response.json()).toHaveProperty('competitions.2.name', "Premier League");
    expect(await response.json()).toHaveProperty('competitions.3.code', "CL");
    expect(await response.json()).toHaveProperty('competitions.3.name', "UEFA Champions League");
})

test('Filter by area', async ({ request }) => {

    const key = Utility.getKey();
    const url = 'http://api.football-data.org/v4/competitions?areas=2077';

    // Send request
    const response = await request.get(
        url, {
        headers: {
            'X-Auth-Token': key
        }
    });

    // Assert: Response code 200
    expect(response.status()).toEqual(200);

    // Assert: Competitions
    expect(await response.json()).toHaveProperty('competitions.1.code', "PL");
    expect(await response.json()).toHaveProperty('competitions.1.name', "Premier League");
    expect(await response.json()).toHaveProperty('competitions.2.code', "CL");
    expect(await response.json()).toHaveProperty('competitions.2.name', "UEFA Champions League");
})