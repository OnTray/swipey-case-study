import test, { expect } from "@playwright/test"
import { Utility } from "../utility";

test('Standings return the correct competition details', async ({ request }) => {

    const key = Utility.getKey();
    const url = 'http://api.football-data.org/v4/competitions/CL/standings';

    // Send request
    const response = await request.get(
        url, {
        headers: {
            'X-Auth-Token': key
        }
    });

    // Assert: Response code 200
    expect(response.status()).toEqual(200);

    // Assert: Area
    expect(await response.json()).toHaveProperty('area.id', 2077);
    expect(await response.json()).toHaveProperty('area.name', 'Europe');

    // Assert: Competition
    expect(await response.json()).toHaveProperty('competition.name', 'UEFA Champions League');
    expect(await response.json()).toHaveProperty('competition.type', 'CUP');
});

test('Status code: Request without valid token will be denied', async ({ request }) => {

    const url = 'http://api.football-data.org/v4/competitions/CL/standings';

    // Send request
    const response = await request.get(url);

    // Assert: Response code 403
    expect(response.status()).toEqual(403);

    // Assert: Error message
    expect(await response.json()).toHaveProperty('message', 'The resource you are looking for is restricted and apparently not within your permissions. Please check your subscription.');
})

test('Status code: Wrong verb should be rejected', async ({ request }) => {

    const url = 'http://api.football-data.org/v4/competitions/CL/standings';

    // Send request
    const response = await request.post(url);

    // Assert: Response code 403
    expect(response.status()).toEqual(405);

    // Assert: Error message
    expect(await response.json()).toHaveProperty('message', 'Method not allowed. Use GET method only.');
})