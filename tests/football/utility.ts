export class Utility {

    /**
     * Retrieve API key
     * @returns API Key
     */
    static getKey(): string {
        if (process.env.FOOTBALL_KEY == undefined) {
            return '';
        } else {
            return process.env.FOOTBALL_KEY;
        }
    }
}